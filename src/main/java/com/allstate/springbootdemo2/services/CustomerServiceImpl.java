package com.allstate.springbootdemo2.services;

import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.entities.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CustomerServiceImpl  implements CustomerService{

    @Autowired
    CustomerRepo customerRepo;
    @Override
    public List<Customer> all() {
        return customerRepo.findAll();
    }

    @Override
    public Optional<Customer> find(int id) {
        return customerRepo.findById(id);
    }

    @Override
    public void save(Customer customer) {
         customerRepo.save(customer);
    }
}
