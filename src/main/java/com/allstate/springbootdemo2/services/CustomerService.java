package com.allstate.springbootdemo2.services;

import com.allstate.springbootdemo2.entities.Customer;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface CustomerService {

   List<Customer> all();

   Optional<Customer> find(int id);
   void save(Customer customer);

}
