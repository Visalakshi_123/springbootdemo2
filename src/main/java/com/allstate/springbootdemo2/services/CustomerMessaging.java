package com.allstate.springbootdemo2.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.Date;

@Service
public class CustomerMessaging {
    @Value("${queuename}")
    private String queuename;
    @Value("${topicname}")
    private String topicname;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendQueueMessaging(String Message){
        String messageTosend= Message + "[" +new Date()+"]";
        jmsTemplate.convertAndSend(queuename,messageTosend);
    }

    public void sendTopicMessaging(String Message){
        jmsTemplate.setPubSubDomain(true);
        String messageTosend= Message + "[" +new Date()+"]";
        jmsTemplate.convertAndSend(topicname,messageTosend);
    }
}
