package com.allstate.springbootdemo2.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;



@Service
public class KafkaPublisher {
    private static final Logger logger= (Logger) LoggerFactory.getLogger(KafkaPublisher.class);
    private static final String TOPIC_NAME="visatopic";

    @Autowired
    private KafkaTemplate<String,String> kafkaTemplate;

    public void sendMessage(String key,String value){
        logger.info(String.format("********** MyPublisher:%s:%s",key,value));
        this.kafkaTemplate.send(TOPIC_NAME,key,value);
    }
}
