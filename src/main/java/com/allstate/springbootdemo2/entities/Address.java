package com.allstate.springbootdemo2.entities;

public class Address {

    private String Line1;
    private String Line2;
    private String Line3;

    public Address(){

    }

    public Address(String line1, String line2, String line3) {
        Line1 = line1;
        Line2 = line2;
        Line3 = line3;
    }

    public String getLine1() {
        return Line1;
    }

    public void setLine1(String line1) {
        Line1 = line1;
    }

    public String getLine2() {
        return Line2;
    }

    public void setLine2(String line2) {
        Line2 = line2;
    }

    public String getLine3() {
        return Line3;
    }

    public void setLine3(String line3) {
        Line3 = line3;
    }
}
