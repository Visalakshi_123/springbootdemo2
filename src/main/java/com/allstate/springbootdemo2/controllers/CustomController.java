package com.allstate.springbootdemo2.controllers;

import com.allstate.springbootdemo2.entities.Address;
import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.services.CustomerMessaging;
import com.allstate.springbootdemo2.services.CustomerService;
import com.allstate.springbootdemo2.services.KafkaPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins="*")
@RequestMapping("api/customer")
public class CustomController {
    @Autowired
    KafkaPublisher kafkaPublisher;
    @Autowired
    CustomerService customerService;
    @Autowired
    CustomerMessaging customerMessaging;
//    JmsTemplate jmsTemplate;
//    @GetMapping("status")
@GetMapping("/status")
    public String getStatus() {

        String message= "Visa Customer Service is running";

     //   String messageToSend = message + " [" + new Date() + "]";

//        jmsTemplate.convertAndSend("dgqueue", messageToSend);
//customerMessaging.sendQueueMessaging(messageToSend);


        return message;

    }

    @PostMapping("/save")
        public void save(@RequestBody Customer customer){


      //  Customer customer=new Customer(2,"Visa",new Address("l1","l2","l3"));
        customerService.save(customer);

        customerMessaging.sendTopicMessaging(customer.getName()+"is created");
    }

    @PostMapping("/find/{id}")
    public Optional<Customer> find(@PathVariable int id) {
    kafkaPublisher.sendMessage("f1","A search for"+id+"was issued");
     return  customerService.find(id);


    }


    @GetMapping("all")
    public List<Customer> all()  {

        return customerService.all();
    }



}
