package com.allstate.springbootdemo2.exceptions;

import java.util.Date;

public class OutOfRangeException extends Exception{

    private Date timestamp=new Date();
    private int severity;


    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int message) {
       this.severity=message;
    }

    public OutOfRangeException(String message, int severity) {
      super(message);
        this.severity = severity;
    }
    public OutOfRangeException(String message) {
        super(message);

    }
}
