package com.allstate.springbootdemo2.entities;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerTest {

    @Test
    public void testConsumerConstructorPopulatesFilelds(){
        Customer customer=new Customer(2,"Visa",new Address("1","2","3"));
        assertEquals(2,customer.getId());
    }
}
