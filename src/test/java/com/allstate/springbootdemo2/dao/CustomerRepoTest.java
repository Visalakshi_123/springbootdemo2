package com.allstate.springbootdemo2.dao;

import com.allstate.springbootdemo2.entities.CustomerRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

public class CustomerRepoTest {
    @Autowired
    CustomerRepo customerRepo;

    @Test
    public void test_rowCount(){
    assertTrue(13<customerRepo.count()) ;
    }
    @Test
    public void test_findByIdOptionalEmpty() {
        assertEquals(
                Optional.empty(), customerRepo.findById(99));
    }


    @Test
    public void test_findByIdNotEmpty() {

        assertNotEquals(Optional.empty(),customerRepo.findById(1));
    }


}
