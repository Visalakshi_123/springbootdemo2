package com.allstate.springbootdemo2.services;

import com.allstate.springbootdemo2.dao.CustomerRepoTest;
import com.allstate.springbootdemo2.entities.Address;
import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.entities.CustomerRepo;
import com.allstate.springbootdemo2.exceptions.OutOfRangeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;


@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class CustomerServiceMockTest {
    @MockBean
    private CustomerRepo customerRepo;

    @Autowired
    CustomerService customerService;

    @Test
    public void testFindByIDThrowsException()
    {

        Exception exception = assertThrows(OutOfRangeException.class, () -> {
            Optional<Customer> customer = customerService.find(0);
        });
    }

    @Test
    public void testFindByIDReturnsCustomer() throws OutOfRangeException {
        Customer customer = new Customer(2, "Deirdre", new Address("l1", "l2", "l3"));

        when(customerRepo.findById(2)).thenReturn(Optional.of(customer));

        assertNotEquals(Optional.empty(), customerService.find(2));

    }

}
