package com.allstate.springbootdemo2.services;


import com.allstate.springbootdemo2.dao.CustomerRepoTest;
import com.allstate.springbootdemo2.entities.Customer;
import com.allstate.springbootdemo2.exceptions.OutOfRangeException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;
@SpringBootTest
public class CustomerServiceTest {


    @Autowired
    CustomerService customerService;

    @Test
    public void testFindByIDThrowsException()
    {

        Exception exception = assertThrows(OutOfRangeException.class, () -> {
            Optional<Customer> customer = customerService.find(0);
        });
    }

//    @Test
//    public void testFindByIDThrowsException()
//    {
//
//        Exception exception = assertThrows(OutOfRangeException.class, () -> {
//            Optional<Customer> customer = customerService.find(0);
//        });
//    }


}